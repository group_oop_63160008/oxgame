/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.nontalee.oxgame1;

/**
 *
 * @author nonta
 */
public class Environment {
    private Box topLeft = new Box();
    private Box topCenter = new Box();
    private Box topRight = new Box();
    private Box middleLeft = new Box();
    private Box middleCenter = new Box();
    private Box middleRight = new Box();
    private Box bottomLeft = new Box();
    private Box bottomCenter = new Box();
    private Box bottomRight = new Box();

    @Override
    public String toString() {

        return String.format(
                "%s %s %s\n%s %s %s\n%s %s %s",
                topLeft,
                topCenter,
                topRight,
                middleLeft,
                middleCenter,
                middleRight,
                bottomLeft,
                bottomCenter,
                bottomRight);
    }

    public Boolean set(Agent agent, int row, int column) {
        switch (row) {
            case 1:
                firstRow(agent, column);
                break;
            case 2:
                secondRow(agent, column);
                break;
            case 3:
                thirdRow(agent, column);
                break;
            default:
                throw new IllegalArgumentException("Your number is not in table location");
        }
        return checkWin();
    }

    private void firstRow(Agent agent, int column) {
        switch (column) {
            case 1:
                topLeft.set(agent);
                break;
            case 2:
                topCenter.set(agent);
                break;
            case 3:
                topRight.set(agent);
                break;
            default:
                throw new IllegalArgumentException("Your number is not in table location");
        }
    }

    private void secondRow(Agent agent, int column) {
        switch (column) {
            case 1:
                middleLeft.set(agent);
                break;
            case 2:
                middleCenter.set(agent);
                break;
            case 3:
                middleRight.set(agent);
                break;
            default:
                throw new IllegalArgumentException("Your number is not in table location");
        }
    }

    private Boolean thirdRow(Agent agent, int column) {
        switch (column) {
            case 1:
                return bottomLeft.set(agent);
            case 2:
                return bottomCenter.set(agent);
            case 3:
                return bottomRight.set(agent);
            default:
                throw new IllegalArgumentException("Your number is not in table location");
        }
    }

    private Boolean checkWin() {
        /**
         * Check All Win Statement
         */
        if ((checkSame(topRight, topLeft, topCenter)) ||
                checkSame(middleRight, middleLeft, middleCenter) ||
                checkSame(bottomRight, bottomLeft, bottomCenter) ||
                checkSame(topRight, middleRight, bottomRight) ||
                checkSame(topCenter, middleCenter, bottomCenter) ||
                checkSame(topLeft, middleLeft, bottomLeft) ||
                checkSame(topRight, middleCenter, bottomLeft) ||
                checkSame(topLeft, middleCenter, bottomRight)) {
            return true;
        }
        return false;
    }

    private Boolean checkSame(Box box1, Box box2, Box box3) {
        if (!box1.getUsed()) {
            return false;
        }
        if (box1.equals(box2) && box1.equals(box3)) {
            return true;
        }
        return false;
    }
}

class Box {
    private String sign = new String();
    private Boolean used = false;

    Box() {
        sign = "-";
    }

    Boolean set(Agent agent) {
        if (used) {
            throw new IllegalStateException("Location is not empty.");
        }
        sign = agent.toString();
        this.used = true;
        return true;
    }

    Boolean getUsed() {
        return used;
    }

    @Override
    public boolean equals(Object obj) {
        return this.sign == obj.toString();
    }

    @Override
    public String toString() {
        return this.sign.intern();
    }
}
