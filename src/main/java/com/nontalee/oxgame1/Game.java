/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.nontalee.oxgame1;

/**
 *
 * @author nonta
 */
import java.util.InputMismatchException;
import java.util.Scanner;

public class Game {
    static private Scanner sc = new Scanner(System.in);
    static Environment table = new Environment();
    static Agent o = new Agent("O");
    static Agent x = new Agent("X");

    public static void main(String[] args) {
        System.out.println("Welcome to OX Game");
        System.out.println(table);
        for (int i = 0; i < 9; i++) {
            try {
                if (i % 2 == 0) {
                    if (turn(o)) {
                        System.out.println(table);
                        System.out.println(">>>O Win<<<");
                        return;
                    }
                } else {
                    if (turn(x)) {
                        System.out.println(table);
                        System.out.println(">>>X Win<<<");
    
                        return;
                    }
                }
            } catch (IllegalStateException e) {
                --i;
                System.err.println(e + " Try again [Select unsed location]");
            } catch (IllegalArgumentException e) {
                --i;
                System.err.println(e + " Try again [Use number in {1, 2 ,3}]");
            } catch (InputMismatchException e) {
                --i;
                System.err.println(e + " Try again [Number only]");
                sc.next();
            }
            System.out.println(table);
        }

        System.out.println(">>>Draw<<<");

    }

    static Boolean turn(Agent agent) {
        System.out.printf("Turn %s\n", agent.toString());
        System.out.println("Please input row, col:");
        int row = sc.nextInt();
        int column = sc.nextInt();
        return table.set(agent, row, column);
    }
}
